let  authorizedCapitalResult:number[];
authorizedCapitalResult=[0,0,0,0,0];
let years:number = 0;
let year1:number = 0;
let year:number[] = [];
const PBARegistrations2015:any = {};
const PBARegistrationsPerYear:any = {};
const fulldate:any = {};
const fs=require('fs');
const csv=require('csv-parser');
fs.createReadStream('../source_data/company_master_data_upto_Mar_2015_Maharashtra.csv')
.pipe(csv())
.on('data',(results:any)=>{
    // 1st Task on AUTHORIZED_CAP
    if(results.AUTHORIZED_CAP < 1e5)
    authorizedCapitalResult[0]+=1;
    if(results.AUTHORIZED_CAP>=1e5 && results.AUTHORIZED_CAP<1e6)
    authorizedCapitalResult[1]+=1;
    if(results.AUTHORIZED_CAP>=1e6 && results.AUTHORIZED_CAP<1e7)
    authorizedCapitalResult[2]+=1;
    if(results.AUTHORIZED_CAP>=1e8 && results.AUTHORIZED_CAP<1e9)
    authorizedCapitalResult[3]+=1;
    if(results.AUTHORIZED_CAP>1e9)
    authorizedCapitalResult[4]+=1;
    // 2nd Task on DATE_OF_REGISTRATION
    const dor = results.DATE_OF_REGISTRATION.split('-');
    //year1 = new Date(`${dor[1]}/${dor[0]}/${dor[2]}`);
    if (dor[2] > 2000 && dor[2] <= 2018) 
    {
      if (!fulldate[dor[2]]) {
        fulldate[dor[2]] = 1;
      }
      else
      {
        fulldate[dor[2]] += 1;
      }
    }
    // 3rd Task on PRINCIPAL_BUSINESS_ACTIVITY
    const principal = results.PRINCIPAL_BUSINESS_ACTIVITY;
    if (dor[2] == 2015) {
      if (!PBARegistrations2015[principal]) {
        PBARegistrations2015[principal] = 1;
      }
      PBARegistrations2015[principal] += 1;
    }

    // 4th Task on YEAR_OF_REGISTRATION and PRINCIPAL_BUSINESS_ACTIVITY
    if (dor[2] >= 2010 && dor[2] <= 2018) {
      if (!PBARegistrationsPerYear[principal]) {
        PBARegistrationsPerYear[principal] = {};
        year = PBARegistrationsPerYear[principal];
        if (!year[dor[2]]) {
          year[dor[2]] = 1;
        }
      } else {
        year = PBARegistrationsPerYear[principal];
        if (!year[dor[2]]) {
          year[dor[2]] = 1;
        } else {
          year[dor[2]] += 1;
        }
      }
    }


})
.on('end',()=>{
    fs.writeFile('../frontend/build/AUTHORIZED_CAP_graph.json',JSON.stringify(authorizedCapitalResult),() => {});
    console.log(authorizedCapitalResult);
    fs.writeFile('../frontend/build/date_of_registration.json',JSON.stringify(fulldate),() => {});
    console.log(fulldate);
    fs.writeFile('../frontend/build/principal_activity.json',JSON.stringify(PBARegistrations2015),() => {});
    console.log(PBARegistrations2015);
    fs.writeFile('../frontend/build/PBA_Registrations_per_year.json',JSON.stringify(PBARegistrationsPerYear),()=>{});
    console.log(PBARegistrationsPerYear);
});


