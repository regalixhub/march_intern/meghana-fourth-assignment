"use strict";
fetch('./build/AUTHORIZED_CAP_graph.json')
    .then(function (response) { return response.json(); })
    .then(function (data) {
    Highcharts.chart('container1', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Authorized Capital'
        },
        xAxis: {
            categories: ['<1L', '1L to 10L', '10L to 1Cr', '10Cr to 100Cr', 'More than 100Cr']
        },
        yAxis: {
            title: {
                text: 'Authorized_Capital in Rupees'
            }
        },
        series: [{
                name: 'Authorized_Capital',
                //data:[data[0],data[1],data[2],data[3],data[4]],
                data: data,
            }]
    });
});
fetch('./build/date_of_registration.json')
    .then(function (response) { return response.json(); })
    .then(function (data) {
    var key = Object.keys(data);
    var value = Object.values(data);
    Highcharts.chart('container2', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'NO_OF_COMPANY_REGISTERED_BY_YEAR'
        },
        xAxis: {
            categories: key,
            title: {
                text: 'YEAR',
            }
        },
        yAxis: {
            title: {
                text: 'NO_OF_COMPANY_REGISTERED'
            }
        },
        series: [{
                name: 'COMPANY_REGISTERED_YEAR',
                //data:[data[0],data[1],data[2],data[3],data[4]],
                data: value,
            }]
    });
});
fetch('./build/principal_activity.json')
    .then(function (response) { return response.json(); })
    .then(function (data) {
    var sorted_key = Object.keys(data).sort(function (a, b) { return data[b] - data[a]; });
    var key = [];
    var no_of_reg = [];
    for (var i = 0; i < 10; i++) {
        key.push(sorted_key[i]);
        no_of_reg.push(data[sorted_key[i]]);
    }
    Highcharts.chart('container3', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top registrations by "Principal Business Activity" for the year 2015'
        },
        xAxis: {
            categories: key,
        },
        yAxis: {
            title: {
                text: 'Number of Company Registrations',
            }
        },
        series: [{
                name: 'Number of Company Registrations',
                //data:[data[0],data[1],data[2],data[3],data[4]],
                data: no_of_reg,
            }]
    });
});
fetch('./build/PBA_Registrations_per_year.json')
    .then(function (resp) { return resp.json(); })
    .then(function (result) {
    var year = [2010];
    for (var i = 0; year[i] <= 2019; i++) {
        year.push(1 + year[i]);
    }
    var objects = [];
    var key1 = Object.keys(result);
    for (var i = 0; i < key1.length; i++) {
        objects.push({
            name: key1[i],
            data: Object.values(result[key1[i]]),
        });
    }
    Highcharts.chart('container4', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'STACKED_BAR_CHART'
        },
        xAxis: {
            categories: year,
            title: {
                text: 'YEAR',
            }
        },
        yAxis: {
            title: {
                text: 'NO_PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_YEAR'
            }
        },
        plotOptions: {
            series: {
                stacking: 'normal',
            },
        },
        series: objects,
    });
});
